package com.busybee.backend.models;

import javax.persistence.*;

@Entity
public class NodeKey {
    @Id
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "node_id", referencedColumnName = "id")
    private Node node;

    private long expiry; // epoch time

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public long getExpiry() {
        return expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }
}
