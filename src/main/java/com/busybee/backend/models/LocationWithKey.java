package com.busybee.backend.models;

public class LocationWithKey {
    Location location;
    String key;

    public LocationWithKey() {
    }

    public LocationWithKey(Location location, String key) {
        this.location = location;
        this.key = key;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
