package com.busybee.backend.models;

public class CountWithKey {
    Count count;
    String key;

    public CountWithKey(){}

    public CountWithKey(Count count, String key) {
        this.count = count;
        this.key = key;
    }

    public Count getCount() {
        return count;
    }

    public void setCount(Count count) {
        this.count = count;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
