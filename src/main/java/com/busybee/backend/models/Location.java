package com.busybee.backend.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String headerName;
    private String subHeaderName;
    private double latitude;
    private double longitude;

    @OneToMany(mappedBy = "location", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private transient  Set<Count> counts;

    @OneToMany(mappedBy = "location", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private transient  Set<Node> nodes;

    public Location() {}

    public Location(long id, String headerName, String subHeaderName, double latitude, double longitude) {
        this.id = id;
        this.headerName = headerName;
        this.subHeaderName = subHeaderName;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public String getSubHeaderName() {
        return subHeaderName;
    }

    public void setSubHeaderName(String subHeaderName) {
        this.subHeaderName = subHeaderName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isMalformed() {
        return (this.headerName == null || this.subHeaderName == null || this.id == 0);
    }
}
