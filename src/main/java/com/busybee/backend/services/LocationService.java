package com.busybee.backend.services;

import com.busybee.backend.models.Location;
import com.busybee.backend.repository.LocationRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {

    private LocationRepository locationRepository;

    @Autowired
    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public Location registerLocation(Location location) {
        if(location.getHeaderName() == null || location.getSubHeaderName() == null || location.getHeaderName().isEmpty() || location.getSubHeaderName().isEmpty()) {
            return null;
        } else {
            return locationRepository.save(location);
        }
    }

    public Location retrieveLocation(long id) {
        return locationRepository.existsById(id) ? locationRepository.findById(id).get() : null;

    }

    public List<Location> retrieveAllLocations() {
        return Lists.newArrayList(locationRepository.findAll());
    }
}
