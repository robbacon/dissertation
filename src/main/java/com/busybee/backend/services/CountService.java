package com.busybee.backend.services;

import com.busybee.backend.models.Count;
import com.busybee.backend.repository.CountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountService {
    private CountRepository countRepository;

    @Autowired
    public CountService(CountRepository countRepository) {
        this.countRepository = countRepository;
    }

    public Count addCount(Count count) {
        return count == null || count.getAmount() < 0 || count.getLocation() == null || count.getLocation().isMalformed() || count.getTimeTaken() <= 0
                ? null : countRepository.save(count);
    }

    public List<Count> getLocationCounts(int id) {
        if(id <= 0) {
            return null;
        } else {
            return countRepository.findByLocation_IdOrderByTimeTaken(new Long(id)).orElse(null);
        }
    }
}
