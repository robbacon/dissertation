package com.busybee.backend.services;

import com.busybee.backend.models.Node;
import com.busybee.backend.models.NodeKey;
import com.busybee.backend.models.Location;
import com.busybee.backend.repository.NodeKeyRepository;
import com.busybee.backend.repository.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.Random;

@Service
public class NodeService {
    private LocationService locationService;
    private NodeRepository nodeRepository;
    private NodeKeyRepository nodeKeyRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public NodeService(LocationService locationService,
                         NodeRepository nodeRepository,
                         NodeKeyRepository nodeKeyRepository,
                         PasswordEncoder passwordEncoder) {
        this.locationService = locationService;
        this.nodeRepository = nodeRepository;
        this.nodeKeyRepository = nodeKeyRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public Node checkNodeExists(long nodeId) {
        return nodeRepository.existsById(nodeId) ? nodeRepository.findById(nodeId).get() : null;
    }

    public Node registerNode(Node node) {
        if(node.getPassword() == null || node.getPassword().isEmpty() || node.getLocation() == null) {
            return null;
        } else {
            node.setPassword(passwordEncoder.encode(node.getPassword()));
            if(node.getLocation().getId() == 0) {
                // Registering location if not ID provided
                Location location = locationService.registerLocation(node.getLocation());
                if(location == null) {
                    return null;
                }
                node.setLocation(location);
            }
            return nodeRepository.save(node);
        }
    }

    public Node updateLocation(Long nodeId, Location location) {
        if(location != null && nodeRepository.existsById(nodeId)) {
            Node node = nodeRepository.findById(nodeId).get();
            if(location.getId() == 0) {
                location = locationService.registerLocation(location);
                if(location == null) {
                    return null;
                }
                node.setLocation(location);
            }
            node.setLocation(location);
            return nodeRepository.save(node);
        } else {
            return null;
        }
    }

    public String createNodeKey(Node node) {
        if(nodeRepository.existsById(node.getId()) && passwordEncoder.matches(node.getPassword(), nodeRepository.findById(node.getId()).get().getPassword())) {
            Optional<NodeKey> nodeKey = nodeKeyRepository.findByNode_Id(node.getId());
            nodeKey.ifPresent(key -> nodeKeyRepository.delete(key));

            NodeKey key = new NodeKey();
            key.setId(generateKey());
            key.setNode(node);
            key.setExpiry(Instant.now().getEpochSecond());
            key = nodeKeyRepository.save(key);
            return key.getId();
        } else {
            return null;
        }
    }

    private String generateKey() {
        int leftCharLimit = 48; // numeral '0'
        int rightCharLimit = 122; // character 'z'
        int targetStringLength = 10;
        Random random = new Random();

        // Generating random alphanumeric key
        return random.ints(leftCharLimit, rightCharLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public Node getNodeFromKey(String key) {
        return nodeKeyRepository.existsById(key) ? nodeKeyRepository.findById(key).get().getNode() : null;
    }

    public boolean checkNodeKey(Node node, String Key) {
        Optional<NodeKey> nodeKey = nodeKeyRepository.findByNode_Id(node.getId());

        return nodeKey.filter(key -> Key.equals(key.getId())).isPresent();
    }

}
