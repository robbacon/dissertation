package com.busybee.backend.controllers;

import com.busybee.backend.models.Node;
import com.busybee.backend.models.Location;
import com.busybee.backend.models.LocationWithKey;
import com.busybee.backend.services.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("node")
public class NodeController {
    @Autowired
    private NodeService nodeService;

    @PostMapping("/verify")
    @ResponseBody
    public ResponseEntity<Node> postRegisterOrVerify(@RequestBody Node node) {
        if (node.getId() == 0 || nodeService.checkNodeExists(node.getId()) == null) {
            node = nodeService.registerNode(node);
        } else {
            node = nodeService.checkNodeExists(node.getId());
        }

        return node == null ? new ResponseEntity<>(null, HttpStatus.BAD_REQUEST) : new ResponseEntity<>(node, HttpStatus.OK) ;
    }

    @PostMapping("/{id}/location")
    @ResponseBody
    public ResponseEntity<Node> postUpdateNodeLocation(@PathVariable int id, @RequestBody LocationWithKey location) {
        return location.getLocation() == null || !nodeService.checkNodeKey(nodeService.checkNodeExists(id),location.getKey())
                ? new ResponseEntity<>(null, HttpStatus.BAD_REQUEST)
                :  new ResponseEntity<>(nodeService.updateLocation(Long.valueOf(id), location.getLocation()), HttpStatus.OK);

    }

    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<String> postGenerateNodeKey(@RequestBody Node node) {
        String key = nodeService.createNodeKey(node);
        return key == null ? new ResponseEntity<>(null, HttpStatus.BAD_REQUEST) :  new ResponseEntity<>(key, HttpStatus.OK);
    }
}
