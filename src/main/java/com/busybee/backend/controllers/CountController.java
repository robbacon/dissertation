package com.busybee.backend.controllers;

import com.busybee.backend.models.CountWithKey;
import com.busybee.backend.services.CountService;
import com.busybee.backend.services.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import com.busybee.backend.models.Count;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("count")
public class CountController {
    @Autowired
    private CountService countService;

    @Autowired
    private NodeService nodeService;

    @PostMapping("/submit")
    @ResponseBody
    public ResponseEntity<Count> postSubmitCount(@RequestBody CountWithKey count) {
        ResponseEntity<Count> response;

        if (nodeService.checkNodeKey(nodeService.getNodeFromKey(count.getKey()),count.getKey())) {
            Count savedCount = countService.addCount(count.getCount());
            if(savedCount == null) {
                response = new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            } else {
                response = new ResponseEntity<>(savedCount, HttpStatus.OK);
            }
        } else {
            response = new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
        return response;
    }

    @GetMapping("/location/{id}")
    @ResponseBody
    public ResponseEntity<List<Count>> getCounts(@PathVariable int id) {
        List<Count> counts = countService.getLocationCounts(id);
        ResponseEntity<List<Count>> response;
        if(counts == null) {
            response = new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        } else {
            response = new ResponseEntity<>(countService.getLocationCounts(id), HttpStatus.OK);
        }
        return response;
    }
}
