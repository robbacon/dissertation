package com.busybee.backend.controllers;

import com.busybee.backend.models.Location;
import com.busybee.backend.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("location")
public class LocationController {

    private LocationService locationService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @PostMapping("/register")
    @ResponseBody
    public ResponseEntity<Location> postRegisterLocation(@RequestBody Location location) {
        Location savedLocation = locationService.registerLocation(location);
        return savedLocation == null ? new ResponseEntity<>(null, HttpStatus.BAD_REQUEST) :  new ResponseEntity<>(savedLocation, HttpStatus.OK)  ;
    }

    @GetMapping("/retrieve/{id}")
    @ResponseBody
    public ResponseEntity<Location> getRetrieveLocation(@PathVariable int id) {
        Location savedLocation = locationService.retrieveLocation(id);
        return savedLocation == null ? new ResponseEntity<>(null, HttpStatus.BAD_REQUEST) :  new ResponseEntity<>(savedLocation, HttpStatus.OK)  ;

    }
}
