package com.busybee.backend.controllers;


import com.busybee.backend.models.Count;
import com.busybee.backend.models.Location;
import com.busybee.backend.services.CountService;
import com.busybee.backend.services.LocationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ViewController {
    @Autowired
    LocationService locationService;

    @Autowired
    CountService countService;

    @GetMapping("/")
    public String index(Model model) {
        List<Location> allLocations = locationService.retrieveAllLocations();

        // Pass attribute to view
        model.addAttribute("allLocations", allLocations);

        // Render index page
        return "index";
    }

    @GetMapping("/location/{id}")
    public String location(@PathVariable Integer id, Model model) {
        List<Count> counts = countService.getLocationCounts(id);

        List<Long> countsTaken = counts.stream().map(Count::getAmount).collect(Collectors.toList());
        List<Long> timesTaken = counts.stream().map(Count::getTimeTaken).collect(Collectors.toList());

        // Pass attributes to view
        model.addAttribute("countsTaken", countsTaken);
        model.addAttribute("timesTaken", timesTaken);
        model.addAttribute("header", (counts.get(0).getLocation().getHeaderName() + " - " + counts.get(0).getLocation().getSubHeaderName()));

        // Render location page
        return "location";
    }
}