package com.busybee.backend.repository;

import com.busybee.backend.models.NodeKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NodeKeyRepository extends CrudRepository<NodeKey, String> {
    Optional<NodeKey> findByNode_Id(long nodeId);

}
