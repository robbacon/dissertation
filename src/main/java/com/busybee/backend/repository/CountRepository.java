package com.busybee.backend.repository;

import com.busybee.backend.models.Count;
import com.busybee.backend.models.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CountRepository extends CrudRepository<Count, Long> {
    Optional<List<Count>> findByLocation_IdOrderByTimeTaken(Long locationId);
}
