Feature: Count Tests

  @BeforeCreateNodeAndKey
  Scenario: Count can be submitted
    Given the count can be submitted
    Then the count can be verified in the database
    And the response is 200

  @BeforeCreateNodeAndKey
  Scenario: Count cannot be submitted malformed
    Given the count cannot be submitted with negative amount
    Then the count can be verified not in the database
    And the response is 400

  @BeforeCreateNodeAndKey
  Scenario: Count cannot be submitted without location
    Given the count cannot be submitted without location
    Then the count can be verified not in the database
    And the response is 400

  @BeforeCreateNodeAndKey
  Scenario: Count cannot be submitted with malformed location
    Given the count cannot be submitted with malformed location
    Then the count can be verified not in the database
    And the response is 400

  @BeforeCreateNodeAndKey
  Scenario: Counts can be retrieved for a location
    Given the count can be submitted
    Then the count can be verified in the database
    Then the count can be retrieved
    And the response is 200

  Scenario: Counts cannot be retrieved for a location that does not exist
    Given the count cannot be retrieved
    And the response is 400


