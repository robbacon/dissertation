Feature: Node Tests

  @BeforeCreateLocations
  Scenario: Node can be registered at existing location
    Given the node can be created with existing location
    Then the node can be verified in the database
    And the response is 200

  Scenario: Node can be registered at new location
    Given the node can be created with new location
    Then the node can be verified in the database
    And the response is 200

  Scenario: Node cannot be registered without a location
    Given the node cannot be created without location
    Then the node can be verified not in the database
    And the response is 400

  @BeforeCreateLocations
  Scenario: Node can update its location with existing
    Given the node can be created with existing location
    Then the node can be verified in the database
    When the node can be updated with existing location
    Then the node can be verified updated in the database
    And the response is 200

  @BeforeCreateLocations
  Scenario: Node can update with a new location
    Given the node can be created with existing location
    Then the node can be verified in the database
    When the node can be updated with new location
    Then the node can be verified updated in the database
    And the response is 200

  @BeforeCreateLocations
  Scenario: Node cannot update without a location
    Given the node can be created with existing location
    Then the node can be verified in the database
    When the node cannot be updated without location
    Then the node can be verified not updated in the database
    And the response is 400

  @BeforeCreateLocations
  Scenario: Node can generate key
    Given the node can be created with existing location
    And the node can be verified in the database
    When the node can generate a key
    Then the key can be verified in the database
    And the response is 200

  @BeforeCreateLocations
  Scenario: Node cannot generate key
    Given the node can be created with existing location
    And the node can be verified in the database
    When the node has the wrong password
    Then the key can be verified not in the database
    And the response is 400
