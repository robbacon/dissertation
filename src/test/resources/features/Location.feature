Feature: Location Tests

  Scenario: Location can be registered
    Given the location can be created
    Then the location can be verified in the database
    And the response is 200

  Scenario: Location cannot be registered malformed
    Given the location cannot be created malformed
    Then the location can be verified not in the database
    And the response is 400

  Scenario: Location can be retrieved
    Given the location can be created
    When the location can be verified in the database
    Then the location can be retrieved
    And the response is 200

  Scenario: Location cannot be retrieved that does not exist
    Given the location cannot be retrieved
    And the response is 400