package com.busybee.backend.integration;

import com.busybee.backend.integration.CucumberBase;
import com.busybee.backend.models.*;
import com.busybee.backend.repository.CountRepository;
import com.busybee.backend.repository.LocationRepository;
import com.busybee.backend.repository.NodeKeyRepository;
import com.busybee.backend.repository.NodeRepository;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@Ignore
public class StepDefs extends CucumberBase {
    @Autowired
    private NodeRepository nodeRepository;
    @Autowired
    private NodeKeyRepository nodeKeyRepository;
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private CountRepository countRepository;

    private int response;

    static ResponseEntity<Location> CREATED_LOCATION;
    static ResponseEntity<Location> CREATED_LOCATION_2;
    static Location CREATED_LOCATION_3;
    static ResponseEntity<Node> CREATED_NODE;
    static ResponseEntity<String> CREATED_KEY;

    ResponseEntity<Node> responseNode;
    ResponseEntity<Node> responseUpdatedNode;
    ResponseEntity<String> responseNodeKey;
    ResponseEntity<Location> responseLocation;
    ResponseEntity<Location> responseLocationNew;
    ResponseEntity<Count> responseCount;
    ResponseEntity<List<Count>> responseCountList;

    private long dbSize;

    private String NODE_PASSWORD = "Password";
    private String LOCATION_HEADER = "Header";
    private String LOCATION_SUB_HEADER = "Sub Header";
    private double LOCATION_LONGITUDE = 50.0;
    private double LOCATION_LATITUDE = 50.0;
    private long COUNT_AMOUNT = 15;
    private long COUNT_TIME_TAKEN = 1235436332;

    @Before("@BeforeCreateLocations")
    public void create_existing_location() {
        Location location = generateLocation();
        Location locationUpdate = generateLocation();

        CREATED_LOCATION = template.postForEntity("/location/register", location, Location.class);

        CREATED_LOCATION_2 = template.postForEntity("/location/register", locationUpdate, Location.class);

        if (CREATED_LOCATION.getStatusCodeValue() == HttpStatus.BAD_REQUEST.value() && CREATED_LOCATION_2.getStatusCodeValue() == HttpStatus.BAD_REQUEST.value()) {
            fail("Couldn't create location");
        }
    }

    @Before("@BeforeCreateNodeAndKey")
    public void create_node() {
        Node node = generateNode(generateLocation());

        CREATED_NODE = template.postForEntity("/node/verify", node, Node.class);
        if (CREATED_NODE.getStatusCodeValue() == HttpStatus.BAD_REQUEST.value()) {
            fail("Couldn't create node");
        }
        CREATED_LOCATION_3 = CREATED_NODE.getBody().getLocation();
        node.setId(CREATED_NODE.getBody().getId());
        node.setLocation(CREATED_NODE.getBody().getLocation());
        CREATED_KEY = template.postForEntity("/node/login", node, String.class);
        if (CREATED_KEY.getStatusCodeValue() == HttpStatus.BAD_REQUEST.value()) {
            fail("Couldn't create node key");
        }
    }

    private Location generateLocation() {
        Location location = new Location();
        location.setHeaderName(LOCATION_HEADER);
        location.setSubHeaderName(LOCATION_SUB_HEADER);
        location.setLongitude(LOCATION_LONGITUDE);
        location.setLatitude(LOCATION_LATITUDE);
        return location;
    }

    private Node generateNode(Location location) {
        Node node = new Node();
        node.setPassword(NODE_PASSWORD);
        node.setLocation(location);
        return node;
    }

        @Given("^the node can be created with existing location$")
    public void the_node_can_be_created_with_existing () throws Throwable {
        Node node = new Node();
        node.setLocation(CREATED_LOCATION.getBody());
        node.setPassword(NODE_PASSWORD);

        responseNode = template.postForEntity("/node/verify", node, Node.class);
        assertEquals(HttpStatus.OK, responseNode.getStatusCode());
        response = responseNode.getStatusCodeValue();
    }

    @Given("^the node can be created with new location$")
    public void the_node_can_be_created_with_new () throws Throwable {
        Node node = new Node();
        node.setLocation(generateLocation());
        node.setPassword(NODE_PASSWORD);

        responseNode = template.postForEntity("/node/verify", node, Node.class);
        assertEquals(HttpStatus.OK, responseNode.getStatusCode());
        response = responseNode.getStatusCodeValue();
    }

    @Given("^the node cannot be created without location$")
    public void the_node_cannot_be_created_without_location () throws Throwable {
        Node node = new Node();
        node.setPassword(NODE_PASSWORD);

        dbSize = nodeRepository.count();
        responseNode = template.postForEntity("/node/verify", node, Node.class);
        assertEquals(responseNode.getStatusCode(), HttpStatus.BAD_REQUEST);
        response = responseNode.getStatusCodeValue();
    }

    @Then("^the node can be verified in the database$")
    public void the_node_can_be_verified_in_the_database () throws Throwable {
        Optional<Node> node = nodeRepository.findById(responseNode.getBody().getId());

        assertTrue(node.isPresent() && responseNode.getBody().getPassword().equals(node.get().getPassword()));
    }

    @Then("^the node can be verified not in the database$")
    public void the_node_can_be_verified_not_in_the_database () throws Throwable {
        long dbSizeAfter = nodeRepository.count();
        assertEquals(dbSize, dbSizeAfter);
    }

    @Then("^the node can be updated with existing location$")
    public void the_node_can_be_updated_with_existing_location () throws Throwable {
        Node node = new Node();
        node.setId(responseNode.getBody().getId());
        node.setLocation(responseNode.getBody().getLocation());
        node.setPassword(NODE_PASSWORD);

        responseNodeKey = template.postForEntity("/node/login", node, String.class);

        LocationWithKey location = new LocationWithKey();
        location.setKey(responseNodeKey.getBody());
        location.setLocation(CREATED_LOCATION_2.getBody());

        responseUpdatedNode = template.postForEntity("/node/" + node.getId() + "/location", location, Node.class);
        assertEquals(HttpStatus.OK, responseUpdatedNode.getStatusCode());
        response = responseUpdatedNode.getStatusCodeValue();
    }

    @Then("^the node can be verified updated in the database$")
    public void the_node_can_be_verified_updated_in_the_database () throws Throwable {
        Optional<Node> node = nodeRepository.findById(responseUpdatedNode.getBody().getId());

        assertTrue(node.isPresent() && responseUpdatedNode.getBody().getPassword().equals(node.get().getPassword()));
        assertNotEquals(responseNode.getBody().getLocation().getId(), node.get().getLocation().getId());
    }

    @When("^the node can be updated with new location$")
    public void the_node_can_be_updated_with_new_location () throws Throwable {
        Node node = new Node();
        node.setId(responseNode.getBody().getId());
        node.setLocation(responseNode.getBody().getLocation());
        node.setPassword(NODE_PASSWORD);

        responseNodeKey = template.postForEntity("/node/login", node, String.class);

        LocationWithKey location = new LocationWithKey();
        location.setKey(responseNodeKey.getBody());
        location.setLocation(generateLocation());

        responseUpdatedNode = template.postForEntity("/node//" + node.getId() + "/location", location, Node.class);
        assertEquals(HttpStatus.OK, responseUpdatedNode.getStatusCode());
        response = responseUpdatedNode.getStatusCodeValue();
    }

    @Then("^the node cannot be updated without location$")
    public void the_node_cannot_be_updated_without_location () throws Throwable {
        responseNodeKey = template.postForEntity("/node/login", responseNode.getBody(), String.class);
        LocationWithKey location = new LocationWithKey();
        location.setKey(responseNodeKey.getBody());

        dbSize = nodeRepository.count();
        responseUpdatedNode = template.postForEntity("/node/" + responseNode.getBody().getId() + "/location", location, Node.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseUpdatedNode.getStatusCode());
        response = responseUpdatedNode.getStatusCodeValue();
    }

    @Then("^the node can be verified not updated in the database$")
    public void the_node_can_be_verified_not_updated_in_the_database () throws Throwable {
        Optional<Node> node = nodeRepository.findById(responseNode.getBody().getId());

        assertTrue(node.isPresent());
        assertEquals(responseNode.getBody().getLocation().getId(), node.get().getLocation().getId());
    }

    @When("^the node can generate a key$")
    public void the_node_can_generate_a_key () throws Throwable {
        Node node = new Node();
        node.setId(responseNode.getBody().getId());
        node.setLocation(responseNode.getBody().getLocation());
        node.setPassword(NODE_PASSWORD);

        responseNodeKey = template.postForEntity("/node/login", node, String.class);
        assertEquals(HttpStatus.OK, responseNodeKey.getStatusCode());

        response = responseNodeKey.getStatusCodeValue();
    }
    @Then("^the key can be verified in the database$")
    public void the_key_can_be_verified_in_the_database () throws Throwable {
        Optional<NodeKey> nodeKey = nodeKeyRepository.findById(responseNodeKey.getBody());

        assertTrue(nodeKey.isPresent());
    }

    @When("^the node has the wrong password$")
    public void the_node_has_the_wrong_password () throws Throwable {
        Node node = responseNode.getBody();
        assert node != null;
        node.setPassword("WrongPassword");

        dbSize = nodeKeyRepository.count();
        responseNodeKey = template.postForEntity("/node/login", node, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseNodeKey.getStatusCode());
        response = responseNodeKey.getStatusCodeValue();
    }

    @Then("^the key can be verified not in the database$")
    public void the_key_can_be_verified_not_in_the_database () throws Throwable {
        long dbSizeNew = nodeKeyRepository.count();
        assertEquals(dbSize, dbSizeNew);
    }

    @Given("^the location can be created$")
    public void the_location_can_be_created () throws Throwable {
        Location location = generateLocation();

        responseLocation = template.postForEntity("/location/register", location, Location.class);
        assertEquals(HttpStatus.OK, responseLocation.getStatusCode());
        response = responseLocation.getStatusCodeValue();
    }

    @Then("^the location can be verified in the database$")
    public void the_location_can_be_verified_in_the_database () throws Throwable {
        Optional<Location> location = locationRepository.findById(responseLocation.getBody().getId());

        assertTrue(location.isPresent());
    }

    @Given("^the location cannot be created malformed$")
    public void the_location_cannot_be_created_malformed() throws Throwable {
        Location location = generateLocation();
        location.setHeaderName(null);
        location.setSubHeaderName(null);

        dbSize = locationRepository.count();
        responseLocation = template.postForEntity("/location/register", location, Location.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseLocation.getStatusCode());
        response = responseLocation.getStatusCodeValue();
    }

    @Then("^the location can be verified not in the database$")
    public void the_location_can_be_verified_not_in_the_database () throws Throwable {
        long dbSizeNew = locationRepository.count();
        assertEquals(dbSize, dbSizeNew);
    }

    @Then("^the location can be retrieved")
    public void the_location_can_be_retrieved () throws Throwable {
        responseLocationNew = template.getForEntity("/location/retrieve/" + responseLocation.getBody().getId() , Location.class);
        assertEquals(HttpStatus.OK, responseLocationNew.getStatusCode());
        assertNotNull(responseLocationNew.getBody());
        response = responseLocationNew.getStatusCodeValue();
    }

    @Given("^the location cannot be retrieved")
    public void the_location_cannot_be_retrieved () throws Throwable {
        responseLocationNew = template.getForEntity("/location/retrieve/" + 0, Location.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseLocationNew.getStatusCode());
        assertNull(responseLocationNew.getBody());
        response = responseLocationNew.getStatusCodeValue();
    }

    @Given("^the count can be submitted$")
    public void the_count_can_be_submitted () throws Throwable {
        Count count = new Count();
        count.setAmount(COUNT_AMOUNT);
        count.setTimeTaken(COUNT_TIME_TAKEN);
        count.setLocation(CREATED_LOCATION_3);

        CountWithKey countWithKey = new CountWithKey();
        countWithKey.setCount(count);
        countWithKey.setKey(CREATED_KEY.getBody());

        responseCount = template.postForEntity("/count/submit", countWithKey, Count.class);
        assertEquals(HttpStatus.OK, responseCount.getStatusCode());
        response = responseCount.getStatusCodeValue();
    }

    @Then("^the count can be verified in the database$")
    public void the_count_can_be_verified_in_the_database () throws Throwable {
        Optional<Count> count = countRepository.findById(responseCount.getBody().getId());

        assertTrue(count.isPresent());
    }

    @Given("^the count cannot be submitted with negative amount$")
    public void the_count_cannot_be_submitted_without_amount () throws Throwable {
        Count count = new Count();
        count.setAmount(-1);
        count.setTimeTaken(COUNT_TIME_TAKEN);
        count.setLocation(CREATED_LOCATION_3);

        CountWithKey countWithKey = new CountWithKey();
        countWithKey.setCount(count);
        countWithKey.setKey(CREATED_KEY.getBody());

        dbSize = countRepository.count();
        responseCount = template.postForEntity("/count/submit", countWithKey, Count.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseCount.getStatusCode());
        response = responseCount.getStatusCodeValue();
    }

    @Then("^the count can be verified not in the database$")
    public void the_count_can_be_verified_not_in_the_database () throws Throwable {
        long dbSizeNew = countRepository.count();
        assertEquals(dbSize, dbSizeNew);
    }

    @Given("^the count cannot be submitted without location$")
    public void the_count_cannot_be_submitted_without_location () throws Throwable {
        Count count = new Count();
        count.setAmount(COUNT_AMOUNT);
        count.setTimeTaken(COUNT_TIME_TAKEN);

        CountWithKey countWithKey = new CountWithKey();
        countWithKey.setCount(count);
        countWithKey.setKey(CREATED_KEY.getBody());

        dbSize = countRepository.count();
        responseCount = template.postForEntity("/count/submit", countWithKey, Count.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseCount.getStatusCode());
        response = responseCount.getStatusCodeValue();
    }


    @Given("^the count cannot be submitted with malformed location$")
    public void the_count_cannot_be_submitted_with_malformed_location () throws Throwable {
        Count count = new Count();
        count.setAmount(COUNT_AMOUNT);
        Location location = CREATED_LOCATION_3;
        location.setId(0);
        count.setLocation(location);

        CountWithKey countWithKey = new CountWithKey();
        countWithKey.setCount(count);
        countWithKey.setKey(CREATED_KEY.getBody());

        dbSize = countRepository.count();
        responseCount = template.postForEntity("/count/submit", countWithKey, Count.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseCount.getStatusCode());
        response = responseCount.getStatusCodeValue();
    }

    @Given("^the count can be retrieved")
    public void the_count_can_be_retrieved () throws Throwable {
        responseCountList = template.exchange("/count/location/" + CREATED_LOCATION_3.getId(),
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Count>>() {
                });
        assertEquals(HttpStatus.OK, responseCount.getStatusCode());
        response = responseCount.getStatusCodeValue();
    }

    @Then("^the count cannot be retrieved")
    public void the_count_cannot_be_retrieved () throws Throwable {
        responseLocationNew = template.getForEntity("/count/location/" + 0, Location.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseLocationNew.getStatusCode());
        assertNull(responseLocationNew.getBody());
        response = responseLocationNew.getStatusCodeValue();
    }

    @And("the response is {int}")
    public void the_response_is (int code) throws Throwable {
        assertThat("status code is incorrect", response, is(code));
    }
}
