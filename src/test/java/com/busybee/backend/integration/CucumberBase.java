package com.busybee.backend.integration;

import com.busybee.backend.BackendApplication;
import io.cucumber.java.en.And;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(classes = BackendApplication.class, webEnvironment = RANDOM_PORT)
@ContextConfiguration
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class CucumberBase {
    @Autowired
    public TestRestTemplate template;

    @LocalServerPort
    public int randomServerPort;

    @Before
    public void contextLoads() throws Exception {
    }


}

