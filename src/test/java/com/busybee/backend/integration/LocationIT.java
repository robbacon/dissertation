package com.busybee.backend.integration;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/Location.feature", glue = {"com.busybee.backend.integration"})
public class LocationIT {
}

