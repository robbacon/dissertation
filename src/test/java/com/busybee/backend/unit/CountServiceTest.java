package com.busybee.backend.unit;

import com.busybee.backend.models.Count;
import com.busybee.backend.models.Location;
import com.busybee.backend.repository.CountRepository;
import com.busybee.backend.services.CountService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CountServiceTest {
    private CountRepository countRepository = Mockito.mock(CountRepository.class);

    private CountService countService;

    @Before
    public void setup() {this.countService = new CountService(countRepository);}

    // addCount
    // add normal count, should be stored return 200 and count stored
    // count missing time or count, return 400
    @Test
    public void addCount() {

        Location location = generateLocationWithId();
        Count countToReturn = generateCountWithId(location);

        when(countRepository.save(any())).thenReturn(countToReturn);

        Count count = new Count();
        count.setLocation(location);
        count.setAmount(12);
        count.setTimeTaken(123131345);

        Count saved = countService.addCount(count);

        assertThat(saved).isEqualTo(countToReturn);

        verify(countRepository).save(count);
    }

    @Test
    public void addCountBadParametersAmount() {
        Location location = generateLocationWithId();

        Count count = new Count();
        count.setAmount(-1);
        count.setTimeTaken(123131345);
        count.setLocation(location);

        Count saved = countService.addCount(count);

        assertThat(saved).isEqualTo(null);

        verify(countRepository, never()).save(count);
    }

    @Test
    public void addCountBadParametersTime() {
        Location location = generateLocationWithId();

        Count count = new Count();
        count.setAmount(12);
        count.setTimeTaken(-123131345);
        count.setLocation(location);

        Count saved = countService.addCount(count);

        assertThat(saved).isEqualTo(null);

        verify(countRepository, never()).save(count);
    }


    @Test
    public void addCountBadParametersLocation() {
        Count count = new Count();
        count.setAmount(12);
        count.setTimeTaken(123131345);
        count.setLocation(null);

        Count saved = countService.addCount(count);

        assertThat(saved).isEqualTo(null);

        verify(countRepository, never()).save(count);
    }

    // getLocationCounts
    // Should get count requested with correct number
    // should return 400, count location not found
    @Test
    public void getLocationCounts() {
        Location location = generateLocationWithId();
        List<Count> countsToReturn = generateMultipleCountsWithId(location);

        when(countRepository.findByLocation_IdOrderByTimeTaken(any())).thenReturn(Optional.of(countsToReturn));


        List<Count> counts = countService.getLocationCounts((int) location.getId());

        assertThat(counts).isEqualTo(countsToReturn);

        verify(countRepository).findByLocation_IdOrderByTimeTaken(location.getId());
    }

    @Test
    public void getLocationCountsNoCounts() {
        Location location = generateLocationWithId();
        List<Count> countsToReturn = new ArrayList<>();

        when(countRepository.findByLocation_IdOrderByTimeTaken(any())).thenReturn(Optional.of(countsToReturn));

        List<Count> counts = countService.getLocationCounts((int) location.getId());

        assertThat(counts).isEmpty();

        verify(countRepository).findByLocation_IdOrderByTimeTaken(location.getId());
    }

    private Location generateLocationWithId() {
        Location locationtoReturn = new Location();
        locationtoReturn.setId(1);
        locationtoReturn.setHeaderName("Test Location Header");
        locationtoReturn.setSubHeaderName("Test Location Sub Header");
        locationtoReturn.setLatitude(50.0000);
        locationtoReturn.setLongitude(-25.0000);
        return locationtoReturn;
    }

    public Count generateCount(Location location) {
        Count count = new Count();
        count.setAmount(12);
        count.setTimeTaken(123131345);
        count.setLocation(location);
        return count;
    }

    public Count generateCountWithId(Location location) {
        Count count = generateCount(location);
        count.setId(1);
        return count;
    }

    private List<Count> generateMultipleCountsWithId(Location location) {
        List<Count> counts = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Count count = generateCount(location);
            count.setId(i+1);
            counts.add(count);
        }
        return counts;
    }
}
