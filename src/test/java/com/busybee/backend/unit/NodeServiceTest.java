package com.busybee.backend.unit;

import com.busybee.backend.models.Node;
import com.busybee.backend.models.NodeKey;
import com.busybee.backend.models.Location;
import com.busybee.backend.repository.NodeKeyRepository;
import com.busybee.backend.repository.NodeRepository;
import com.busybee.backend.services.NodeService;
import com.busybee.backend.services.LocationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class NodeServiceTest {
    protected LocationService locationService = Mockito.mock(LocationService.class);
    protected PasswordEncoder passwordEncoder = Mockito.mock(PasswordEncoder.class);
    private NodeRepository nodeRepository = Mockito.mock(NodeRepository.class);
    private NodeKeyRepository nodeKeyRepository = Mockito.mock(NodeKeyRepository.class);

    private NodeService nodeService;

    @Before
    public void setup() {this.nodeService = new NodeService(locationService, nodeRepository, nodeKeyRepository, passwordEncoder);}

    @Test
    public void checkNodeExists() {
        Location location = generateLocationWithId();

        Node nodeToCheck = generateNodeWithId(location);

        when(nodeRepository.existsById(any())).thenReturn(true);
        when(nodeRepository.findById(any())).thenReturn(java.util.Optional.of(nodeToCheck));

        Node node = nodeService.checkNodeExists(1);

        assertThat(node.getId()).isEqualTo(1);
        assertThat(node.getPassword()).isEqualTo("WvCTgh4a");
        assertThat(node.getLocation()).isEqualTo(location);

        verify(nodeRepository).existsById(1L);
        verify(nodeRepository).findById(1L);
    }

    @Test
    public void checkNodeExistsBadParameters() {
        when(nodeRepository.existsById(any())).thenReturn(false);

        Node node = nodeService.checkNodeExists(1);

        assertThat(node).isEqualTo(null);

        verify(nodeRepository).existsById(1L);
        verify(nodeRepository, never()).findById(1L);
    }

    @Test
    public void registerNode() {
        Location location = generateLocationWithId();

        Node nodeToReturn = generateNodeWithId(location);

        when(nodeRepository.save(any())).thenReturn(nodeToReturn);
        when(passwordEncoder.encode(any())).thenReturn("WvCTgh4a");

        Node node = new Node();
        node.setPassword("Password");
        node.setLocation(location);

        Node saved = nodeService.registerNode(node);

        assertThat(saved.getId()).isEqualTo(1);
        assertThat(saved.getPassword()).isNotEqualTo("Password");
        assertThat(saved.getPassword()).isEqualTo("WvCTgh4a");
        assertThat(saved.getLocation()).isEqualTo(location);

        verify(nodeRepository).save(node);
    }

    @Test
    public void registerNodeNewLocation() {
        Location locationtoReturn = generateLocationWithId();

        Node nodeToReturn = generateNodeWithId(locationtoReturn);

        when(locationService.registerLocation(any())).thenReturn(locationtoReturn);
        when(nodeRepository.save(any())).thenReturn(nodeToReturn);
        when(passwordEncoder.encode(any())).thenReturn("WvCTgh4a");

        Location location = new Location();
        location.setHeaderName("Test Location Header");
        location.setSubHeaderName("Test Location Sub Header");
        location.setLatitude(50.0000);
        location.setLongitude(-25.0000);

        Node node = new Node();
        node.setPassword("Password");
        node.setLocation(location);

        Node saved = nodeService.registerNode(node);

        assertThat(saved.getId()).isEqualTo(1);
        assertThat(saved.getPassword()).isNotEqualTo("Password");
        assertThat(saved.getPassword()).isEqualTo("WvCTgh4a");
        assertThat(saved.getLocation()).isEqualTo(locationtoReturn);

        verify(locationService).registerLocation(location);
        verify(nodeRepository).save(node);
    }

    @Test
    public void registerNodeBadLocationParameters() {
        when(locationService.registerLocation(any())).thenReturn(null);

        Location location = new Location();
        location.setLatitude(50.0000);
        location.setLongitude(-25.0000);

        Node node = new Node();
        node.setPassword("Password");
        node.setLocation(location);

        Node saved = nodeService.registerNode(node);

        assertThat(saved).isEqualTo(null);

        verify(nodeRepository, never()).save(node);
    }

    @Test
    public void registerNodeBadNodeParameters() {
        Node node = new Node();

        Node saved = nodeService.registerNode(node);

        assertThat(saved).isEqualTo(null);

        verify(nodeRepository, never()).save(node);
    }

    @Test
    public void updateLocation() {
        Location locationtoReturn = generateLocationWithId();

        Node nodeToReturn = generateNodeWithId(locationtoReturn);

        when(nodeRepository.existsById(any())).thenReturn(true);
        when(nodeRepository.findById(any())).thenReturn(java.util.Optional.of(nodeToReturn));
        when(nodeRepository.save(any())).thenReturn(nodeToReturn);

        Node saved = nodeService.updateLocation(1L, locationtoReturn);

        assertThat(saved.getId()).isEqualTo(1);
        assertThat(saved.getPassword()).isNotEqualTo("Password");
        assertThat(saved.getPassword()).isEqualTo("WvCTgh4a");
        assertThat(saved.getLocation()).isEqualTo(locationtoReturn);

        verify(nodeRepository).save(nodeToReturn);
    }

    @Test
    public void updateLocationNewLocation() {
        Location locationtoReturn = generateLocationWithId();

        Node nodeToReturn = generateNodeWithId(locationtoReturn);

        when(nodeRepository.existsById(any())).thenReturn(true);
        when(nodeRepository.findById(any())).thenReturn(java.util.Optional.of(nodeToReturn));
        when(locationService.registerLocation(any())).thenReturn(locationtoReturn);
        when(nodeRepository.save(any())).thenReturn(nodeToReturn);

        Location location = new Location();
        location.setHeaderName("Test Location Header");
        location.setSubHeaderName("Test Location Sub Header");
        location.setLatitude(50.0000);
        location.setLongitude(-25.0000);

        Node saved = nodeService.updateLocation(1L, location);

        assertThat(saved.getId()).isEqualTo(1);
        assertThat(saved.getPassword()).isNotEqualTo("Password");
        assertThat(saved.getPassword()).isEqualTo("WvCTgh4a");
        assertThat(saved.getLocation()).isEqualTo(locationtoReturn);

        verify(locationService).registerLocation(location);
        verify(nodeRepository).save(nodeToReturn);
    }


    @Test
    public void updateLocationBadParameters() {
        when(locationService.registerLocation(any())).thenReturn(null);

        Location location = new Location();
        location.setLatitude(50.0000);
        location.setLongitude(-25.0000);

        Node node = new Node();
        node.setId(1L);
        node.setPassword("Password");
        node.setLocation(location);

        when(nodeRepository.existsById(any())).thenReturn(true);
        when(nodeRepository.findById(any())).thenReturn(java.util.Optional.of(node));
        Node saved = nodeService.updateLocation(1L, location);

        assertThat(saved).isEqualTo(null);

        verify(nodeRepository, never()).save(node);
    }

    @Test
    public void createNodeKey() {
        Location location = generateLocationWithId();
        Node nodeToReturn = generateNodeWithId(location);
        NodeKey nodeKey = generateNodeKey(nodeToReturn);

        when(passwordEncoder.matches(any(),any())).thenReturn(true);
        when(nodeRepository.existsById(any())).thenReturn(true);
        when(nodeRepository.findById(any())).thenReturn(java.util.Optional.of(nodeToReturn));
        when(nodeKeyRepository.save(any())).thenReturn(nodeKey);

        Node node = new Node();
        node.setId(1L);
        node.setPassword("Password");
        node.setLocation(location);

        String key = nodeService.createNodeKey(node);

        assertThat(key).isNotEqualTo(null);

        verify(passwordEncoder).matches("Password", "WvCTgh4a");
        verify(nodeRepository).existsById(node.getId());
        verify(nodeRepository).findById(node.getId());
    }

    @Test
    public void createNodeKeyBadParameters() {
        Location location = generateLocationWithId();
        Node nodeToReturn = generateNodeWithId(location);

        when(nodeRepository.existsById(any())).thenReturn(true);
        when(nodeRepository.findById(any())).thenReturn(java.util.Optional.of(nodeToReturn));
        when(passwordEncoder.matches(any(),any())).thenReturn(false);

        Node node = new Node();
        node.setId(1L);
        node.setPassword("Password");
        node.setLocation(location);

        String key = nodeService.createNodeKey(node);

        assertThat(key).isEqualTo(null);

        verify(nodeRepository, never()).save(any());
    }

    @Test
    public void getNodeFromKey() {
        Location location = generateLocationWithId();
        Node node = generateNodeWithId(location);
        NodeKey nodeKey = generateNodeKey(node);

        when(nodeKeyRepository.existsById(any())).thenReturn(true);
        when(nodeKeyRepository.findById(any())).thenReturn(java.util.Optional.of(nodeKey));

        Node found = nodeService.getNodeFromKey("haAWDfcba7ad");

        assertThat(node).isEqualTo(found);

        verify(nodeKeyRepository).existsById(nodeKey.getId());
        verify(nodeKeyRepository).findById(nodeKey.getId());
    }

    @Test
    public void getNodeFromKeyBadParameters() {
        when(nodeKeyRepository.existsById(any())).thenReturn(false);

        Node node = nodeService.getNodeFromKey("haAWDfcba7ad");

        assertThat(node).isEqualTo(null);

        verify(nodeKeyRepository).existsById("haAWDfcba7ad");
        verify(nodeKeyRepository, never()).findById("haAWDfcba7ad");
    }

    @Test
    public void checkNodeKey() {
        Location location = generateLocationWithId();
        Node node = generateNodeWithId(location);
        NodeKey nodeKey = generateNodeKey(node);

        when(nodeKeyRepository.findByNode_Id(anyLong())).thenReturn(java.util.Optional.of(nodeKey));

        boolean response = nodeService.checkNodeKey(node ,"haAWDfcba7ad");

        assertThat(response).isEqualTo(true);

        verify(nodeKeyRepository).findByNode_Id(node.getId());
    }

    @Test
    public void checkNodeKeyBadParameters() {
        Location location = generateLocationWithId();
        Node node = generateNodeWithId(location);
        NodeKey nodeKey = generateNodeKey(node);

        when(nodeKeyRepository.findByNode_Id(anyLong())).thenReturn(java.util.Optional.of(nodeKey));

        boolean response = nodeService.checkNodeKey(node ,"wrongkey");

        assertThat(response).isEqualTo(false);

        verify(nodeKeyRepository).findByNode_Id(node.getId());
    }

    private Node generateNodeWithId(Location location) {
        Node nodeToReturn = new Node();
        nodeToReturn.setId(1);
        nodeToReturn.setPassword("WvCTgh4a");
        nodeToReturn.setLocation(location);
        return nodeToReturn;
    }

    private Location generateLocationWithId() {
        Location locationtoReturn = new Location();
        locationtoReturn.setId(1);
        locationtoReturn.setHeaderName("Test Location Header");
        locationtoReturn.setSubHeaderName("Test Location Sub Header");
        locationtoReturn.setLatitude(50.0000);
        locationtoReturn.setLongitude(-25.0000);

        return locationtoReturn;
    }

    private NodeKey generateNodeKey(Node node) {
        NodeKey nodeKey = new NodeKey();
        nodeKey.setId("haAWDfcba7ad");
        nodeKey.setExpiry(113124454325L);
        nodeKey.setNode(node);
        return nodeKey;
    }
}
