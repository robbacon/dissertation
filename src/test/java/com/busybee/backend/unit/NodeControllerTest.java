package com.busybee.backend.unit;

import com.busybee.backend.controllers.NodeController;
import com.busybee.backend.models.Location;
import com.busybee.backend.services.NodeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(MockitoJUnitRunner.class)
public class NodeControllerTest {
    private MockMvc mockMvc;

    @InjectMocks
    private NodeController nodeController;

    @Mock
    private NodeService nodeService;


    @Before
    public void before() {
        mockMvc = MockMvcBuilders.standaloneSetup(nodeController).build();
    }

    @Test
    public void postRegisterOrVerify() {
        /*Location locationToReturn = new Location();
        locationToReturn.setId(1);
        locationToReturn.setHeaderName("Test Location Header");
        locationToReturn.setSubHeaderName("Test Location Sub Header");
        locationToReturn.setLatitude(50.0000);
        locationToReturn.setLongitude(-25.0000);

        when(nodeService.registerNode(any())).thenReturn(locationToReturn);

        String jsonString = asJsonString(locationToReturn);
        MockHttpServletResponse response = mockMvc.perform(post("/location/register")
                .content(jsonString)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
        assertEquals(response.getStatus(), HttpStatus.OK.value()); */
    }

    @Test
    public void postRegisterOrVerifyBadParameters() {

    }

    @Test
    public void postUpdateNodeLocation() {

    }

    @Test
    public void postUpdateNodeLocationBadNode() {

    }

    @Test
    public void postUpdateNodeLocationBadLocation() {

    }

    @Test
    public void postGenerateNodeKey() {

    }

    @Test
    public void postGenerateNodeKeyBadNode() {

    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
