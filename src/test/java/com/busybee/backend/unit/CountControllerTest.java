package com.busybee.backend.unit;

import com.busybee.backend.controllers.CountController;
import com.busybee.backend.controllers.LocationController;
import com.busybee.backend.models.Count;
import com.busybee.backend.models.CountWithKey;
import com.busybee.backend.models.Location;
import com.busybee.backend.models.Node;
import com.busybee.backend.services.CountService;
import com.busybee.backend.services.LocationService;
import com.busybee.backend.services.NodeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(MockitoJUnitRunner.class)
public class CountControllerTest {
    private MockMvc mockMvc;

    @InjectMocks
    private CountController countController;

    @Mock
    private CountService countService;
    @Mock
    private NodeService nodeService;

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.standaloneSetup(countController).build();
    }

    @Test
    public void postSubmitCount() throws Exception {
        Location location = generateLocationWithId();
        Count countToReturn = generateCount(location);
        CountWithKey countWithKey = new CountWithKey(countToReturn, "adWDCAS2a");

        when(nodeService.checkNodeKey(any(), any())).thenReturn(true);
        when(nodeService.getNodeFromKey(any())).thenReturn(new Node());
        when(countService.addCount(any())).thenReturn(countToReturn);

        String jsonString = asJsonString(countWithKey);
        MockHttpServletResponse response = mockMvc.perform(post("/count/submit")
                .content(jsonString)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
        assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void postSubmitCountBadKey() throws Exception {
        Location location = generateLocationWithId();
        Count countToReturn = generateCount(location);
        CountWithKey countWithKey = new CountWithKey(countToReturn, "adWDCAS2a");

        when(nodeService.checkNodeKey(any(), any())).thenReturn(false);
        when(nodeService.getNodeFromKey(any())).thenReturn(new Node());

        String jsonString = asJsonString(countWithKey);
        MockHttpServletResponse response = mockMvc.perform(post("/count/submit")
                .content(jsonString)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    public void getCounts() throws Exception {
        Location location = generateLocationWithId();
        Count countToReturn = generateCount(location);
        Count countToReturn2 = generateCount(location);

        List<Count> counts = new ArrayList<>();
        counts.add(countToReturn);
        counts.add(countToReturn2);

        when(countService.getLocationCounts(anyInt())).thenReturn(counts);

        MockHttpServletResponse response = mockMvc.perform(get("/count/location/1"))
                .andReturn().getResponse();

        assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void getCountsBadId() throws Exception {
        List<Count> counts = new ArrayList<>();

        when(countService.getLocationCounts(anyInt())).thenReturn(counts);

        MockHttpServletResponse response = mockMvc.perform(get("/count/location/1"))
                .andReturn().getResponse();

        assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    private Location generateLocationWithId() {
        Location locationtoReturn = new Location();
        locationtoReturn.setId(1);
        locationtoReturn.setHeaderName("Test Location Header");
        locationtoReturn.setSubHeaderName("Test Location Sub Header");
        locationtoReturn.setLatitude(50.0000);
        locationtoReturn.setLongitude(-25.0000);
        return locationtoReturn;
    }

    public Count generateCount(Location location) {
        Count count = new Count();
        count.setAmount(12);
        count.setTimeTaken(123131345);
        count.setLocation(location);
        return count;
    }

    public Count generateCountWithId(Location location) {
        Count count = generateCount(location);
        count.setId(1);
        return count;
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
