package com.busybee.backend.unit;

import com.busybee.backend.models.Location;
import com.busybee.backend.repository.LocationRepository;
import com.busybee.backend.services.LocationService;
import org.junit.Before;
import org.junit.Test;

import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;

public class LocationServiceTest {

    private LocationRepository locationRepository = Mockito.mock(LocationRepository.class);
    private LocationService locationService;

    @Before
    public void setup() {this.locationService = new LocationService(locationRepository);}

    @Test
    public void registerLocation() {
        Location locationToSave = new Location();
        locationToSave.setHeaderName("Test Location Header");
        locationToSave.setSubHeaderName("Test Location Sub Header");
        locationToSave.setLatitude(50.0000);
        locationToSave.setLongitude(-25.0000);

        when(locationRepository.save(any())).thenReturn(locationToSave);
        Location saved = locationService.registerLocation(locationToSave);
        assertThat(saved.getHeaderName()).isEqualTo("Test Location Header");
        verify(locationRepository).save(locationToSave);
    }

    @Test
    public void registerLocationBadParameters() {
        Location locationToSave = new Location();
        locationToSave.setLatitude(50.0000);
        locationToSave.setLongitude(-25.0000);

        when(locationRepository.save(any())).thenReturn(locationToSave);
        Location saved = locationService.registerLocation(locationToSave);
        assertThat(saved).isEqualTo(null);
        verify(locationRepository, never()).save(locationToSave);
    }

    @Test
    public void retrieveLocation() {
        Location locationToRetrieve = new Location();
        locationToRetrieve.setId(1);
        locationToRetrieve.setHeaderName("Test Location Header");
        locationToRetrieve.setSubHeaderName("Test Location Sub Header");
        locationToRetrieve.setLatitude(50.0000);
        locationToRetrieve.setLongitude(-25.0000);

        when(locationRepository.existsById(any())).thenReturn(true);
        when(locationRepository.findById(any())).thenReturn(java.util.Optional.of(locationToRetrieve));

        Location saved = locationService.retrieveLocation(1);
        assertThat(saved.getId()).isEqualTo(1L);
        verify(locationRepository).findById(1L);
    }
    @Test
    public void retrieveLocationBadParameters() {
        Location locationToRetrieve = new Location();
        locationToRetrieve.setHeaderName("Test Location Header");
        locationToRetrieve.setSubHeaderName("Test Location Sub Header");
        locationToRetrieve.setLatitude(50.0000);
        locationToRetrieve.setLongitude(-25.0000);

        when(locationRepository.findById(any())).thenReturn(null);

        Location saved = locationService.registerLocation(locationToRetrieve);
        assertThat(saved).isEqualTo(null);
        verify(locationRepository, never()).findById(1L);
    }

    @Test
    public void retrieveAllLocations() {
        Location locationToRetrieve = new Location();
        locationToRetrieve.setId(1);
        locationToRetrieve.setHeaderName("Test Location Header");
        locationToRetrieve.setSubHeaderName("Test Location Sub Header");
        locationToRetrieve.setLatitude(50.0000);
        locationToRetrieve.setLongitude(-25.0000);

        Location locationToRetrieve1 = new Location();
        locationToRetrieve.setId(2);
        locationToRetrieve.setHeaderName("Test Location Header");
        locationToRetrieve.setSubHeaderName("Test Location Sub Header");
        locationToRetrieve.setLatitude(50.0000);
        locationToRetrieve.setLongitude(-25.0000);

        List<Location> locations = new ArrayList<>();
        locations.add(locationToRetrieve);
        locations.add(locationToRetrieve1);

        when(locationRepository.findAll()).thenReturn(locations);
        List<Location> saved = locationService.retrieveAllLocations();
        assertThat(saved).isEqualTo(locations);
        verify(locationRepository).findAll();
    }

}

