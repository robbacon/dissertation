package com.busybee.backend.unit;

import com.busybee.backend.controllers.LocationController;
import com.busybee.backend.models.Location;
import com.busybee.backend.services.LocationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(MockitoJUnitRunner.class)
public class LocationControllerTest {
    private MockMvc mockMvc;

    @InjectMocks
    private LocationController locationController;

    @Mock
    private LocationService locationService;


    @Before
    public void before() {
        mockMvc = MockMvcBuilders.standaloneSetup(locationController).build();
    }

    @Test
    public void postRegisterLocation() throws Exception {
        Location locationToReturn = new Location();
        locationToReturn.setId(1);
        locationToReturn.setHeaderName("Test Location Header");
        locationToReturn.setSubHeaderName("Test Location Sub Header");
        locationToReturn.setLatitude(50.0000);
        locationToReturn.setLongitude(-25.0000);

        when(locationService.registerLocation(any())).thenReturn(locationToReturn);

        String jsonString = asJsonString(locationToReturn);
        MockHttpServletResponse response = mockMvc.perform(post("/location/register")
                .content(jsonString)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
        assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    @Test
    public void postRegisterLocationBadLocation() throws Exception {
        Location locationToReturn = new Location();
        locationToReturn.setId(1);
        locationToReturn.setLatitude(50.0000);
        locationToReturn.setLongitude(-25.0000);

        when(locationService.registerLocation(any())).thenReturn(null);

        String jsonString = asJsonString(locationToReturn);
        MockHttpServletResponse response = mockMvc.perform(post("/location/register")
                .content(jsonString)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void getRetrieveLocation() throws Exception {
        Location locationToReturn = new Location();
        locationToReturn.setId(1);
        locationToReturn.setHeaderName("Test Location Header");
        locationToReturn.setSubHeaderName("Test Location Sub Header");
        locationToReturn.setLatitude(50.0000);
        locationToReturn.setLongitude(-25.0000);

        when(locationService.retrieveLocation(anyLong())).thenReturn(locationToReturn);

        MockHttpServletResponse response = mockMvc.perform(get("/location/retrieve/" + locationToReturn.getId()))
                .andReturn().getResponse();
        assertEquals(response.getStatus(), HttpStatus.OK.value());
    }
    @Test
    public void getRetrieveLocationBadLocation() throws Exception {
        when(locationService.retrieveLocation(anyLong())).thenReturn(null);

        MockHttpServletResponse response = mockMvc.perform(get("/location/retrieve/1"))
                .andReturn().getResponse();
        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
